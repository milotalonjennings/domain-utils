<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

// require "vendor/autoload.php";
require "vendor/dns.inc.php";
require "include/utils.php";

define( 'SCRIPT_PATH', dirname(__FILE__) );

$timer_start_global = microtime(true);

setlocale(LC_CTYPE, "en_US.UTF-8");
date_default_timezone_set('America/Los_Angeles');

$info = array();
$data = array();

// Determine if a domain has been supplied from a query string or the command line
parse_str($_SERVER['QUERY_STRING'], $qstrings);

$domain = null;
if(isset($qstrings) && array_key_exists('domain', $qstrings) && $qstrings['domain']){
	$domain = $qstrings['domain']; 
	$info['input_type'] = 'qstring';
}
if(isset($argv)){
	if($argv[1]){
		$domain = $argv[1];
		$info['input_type'] = 'cli';
	}
}

if(isset($qstrings) && array_key_exists('info',$qstrings) && array_key_exists('no-cache', $qstrings)){
	// echo "clearing cache for $domain";
	purge_cache($domain, $qstrings['info']);
	purge_cache($domain, 'domain_exists');
}
if(isset($qstrings) && array_key_exists('info',$qstrings) && array_key_exists('purge_cache', $qstrings)){
	// echo "<pre>clearing all cache</pre>";
	purge_cache();
}

// Normalize the domain (remove protocol and subfolders from the string)
$domain = normalize_domain($domain);
// Check if the domain is valid (resolves somewhere and no weird characters in it) 
if(validate_domain($domain)){
	$data = get_cached_domain_info($domain, 'domain_exists', $hours = 3);
	// $data = domain_exists($domain);
	if($data['exists']['success']){
		// Check if domain resolves
		$data['success'] = true;
	} else {
		$data['success'] = false;
		$data['error'] = true;
		if(isset($qstrings) && array_key_exists('info',$qstrings) && $qstrings['info'] == 'status_code'){
			$status_code = check_domain($domain, true);
			$data['status_code'] = $status_code;
		}
		$data['message'][] = "${domain} does not seem to resolve";
	}
} else {
	if(!$domain){
		$data['success'] = false;
		$data['error'] = true;
		$data['message'][] = "Please specify a domain name";
	} else {
		$data['success'] = false;
		$data['error'] = true;
		$data['message'] = "${domain} is not a valid domain name";
	}
}

if($data['success'] == true){
	if(isset($qstrings) && array_key_exists('info',$qstrings) && $qstrings['info'] == 'ip'){
		$ip_data = get_cached_domain_info($domain, 'ip');
		$data['data']['ip'] = $ip_data['data'];
	} elseif(isset($qstrings) && array_key_exists('info',$qstrings) && $qstrings['info'] == 'ssl_expiration'){
		// $data['data'] = get_ssl_domain_expiration('https://'.$domain);
		$ssl_expiration_data = get_cached_domain_info($domain, 'ssl_expiration');
		$data = array_merge($data, $ssl_expiration_data);
	} elseif(isset($qstrings) && array_key_exists('info',$qstrings) && $qstrings['info'] == 'name_server'){
		$name_server_data = get_cached_domain_info($domain, 'name_server', 3);
		$data = array_merge($data, $name_server_data);
	} elseif(isset($qstrings) && array_key_exists('info',$qstrings) && $qstrings['info'] == 'cname'){
		$cname_data = get_cached_domain_info($domain, 'cname', 3);
		$data['data']['cname'] = $cname_data['data'];
	} else {
		$data['success'] = false;
		$data['error'] = true;
		$data['message'] = "No info requested for ${domain}";
	}
	unset($data['exists']);
	echo json_encode($data);
} else {
	unset($data['exists']);
	echo json_encode($data);
}
$global_exec_time = round(microtime(true) - $timer_start_global, 3);
// echo "<pre>Global \$exec_time: " . print_r($global_exec_time, true) . "</pre>";


// ?getdata&data[]=ip&data[]=ssl_expiration