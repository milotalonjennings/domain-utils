<?php

function normalize_domain($domain){
	// Remove protocol and subfolders from the string
	return parse_url('http://' . str_replace(array('https://', 'http://'), '', $domain), PHP_URL_HOST);
}
function get_tld($domain){
	$domain = normalize_domain($domain);
	// Remove subdomain from the domain string
    $array = explode(".", $domain);
    $tld = (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "") . "." . $array[count($array) - 1];
    return $tld;
}
function validate_domain($domain){
	// Check if the domain is valid (no weird characters in it)
	if(filter_var('http://'.$domain, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)){
		return true;
	}
	return false;
}

function get_ssl_domain_expiration($ssl_domain){
	$i = 0;
	$ssl_domain_data = array();

	$domain = parse_url($ssl_domain, PHP_URL_HOST);
	$g = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));
	$r = stream_socket_client("ssl://${domain}:443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $g);
	$cert = stream_context_get_params($r);
	$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
	
	$current_date = new DateTime(date('Y-m-d'), new DateTimeZone('America/Los_Angeles'));
	$expiration_date = new DateTime(date('Y-m-d', $certinfo['validTo_time_t']), new DateTimeZone('America/Los_Angeles'));
	$interval = $current_date->diff($expiration_date);
	$days_left = (int)$interval->format('%a');
	
	$ssl_domain_data = array(
	'domain' => $domain,
	'expiration_date' => $expiration_date->format('m/d/Y'),
	'days_left' => $days_left,
	'ip' => get_ip($domain)
	);

	return $ssl_domain_data;
}

function get_ip($domain){
	if($domain){
		$dns_server="8.8.8.8";
		$question=$domain;

		$type="A";
		$dns_query = new DNSQuery($dns_server);

		$result = $dns_query->Query($question,$type); // do the query

		// Trap Errors
		if ( ($result===false) || ($dns_query->error!=0) ) { // error occured
			echo $dns_query->lasterror;
			exit();
		}

		// Process Results
		$result_count = $result->count; // number of results returned
		$ip = null;
		for ($i=0; $i<$result_count; $i++){
			if ($result->results[$i]->typeid=="A") {
				$ip = $result->results[$i]->data;
				break;
			}
		}
		return $ip;
	} else {
		return false;
	}
}

function get_cname($domain){
	if($domain){
		$dns_server="8.8.8.8";
		$question=$domain;

		$type="CNAME";
		$dns_query = new DNSQuery($dns_server);

		$result = $dns_query->Query($question,$type); // do the query
		// Trap Errors
		if ( ($result===false) || ($dns_query->error!=0) ) { // error occured
			echo $dns_query->lasterror;
			exit();
		}

		// Process Results
		$result_count = $result->count; // number of results returned
		$cname = null;
		for ($i=0; $i<$result_count; $i++){
			if ($result->results[$i]->typeid=="CNAME") {
				$cname = $result->results[$i]->data;
				break;
			}
		}
		return $cname;
	} else {
		return false;
	}
}

function domain_exists($domain){
	$ip = get_cached_domain_info($domain, 'ip');
	if(array_key_exists('data', $ip) && $ip['data']){
		$data['exists']['success'] = true;
		$data['exists']['https'] = check_domain("https://".$domain);
		$data['exists']['httpswww'] = check_domain("https://www.".$domain);
		if($data['exists']['https'] || $data['exists']['httpswww']){
			$data['ssl'] = true;
		}
	} else {
		$data['exists']['success'] = false;
		$data['success'] = false;
	}
	return $data;
}
function check_domain($url, $return_status_code = false){
	$c=curl_init();
	curl_setopt($c,CURLOPT_URL,$url);
	curl_setopt($c,CURLOPT_HEADER,1);//get the header
	curl_setopt($c,CURLOPT_NOBODY,1);//and *only* get the header
	curl_setopt($c,CURLOPT_RETURNTRANSFER,1);//get the response as a string from curl_exec(), rather than echoing it
	curl_setopt($c,CURLOPT_FRESH_CONNECT,1);//don't use a cached version of the url
	curl_setopt($c,CURLOPT_TIMEOUT,5);
	$output = curl_exec($c);
	$httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
	$redirect_url = curl_getinfo($c, CURLINFO_REDIRECT_URL);
	if(isset($redirect_url)){
		if (strpos($redirect_url, $url) !== false) {
			return true;
		}
	}
	if($return_status_code){
		return $httpcode;
	}
	if($httpcode == '200'){
		return true;
	} else {
		return false;
	}
}

function get_name_server($domain){
	$dns_server="8.8.8.8";

	// Make sure it's the top level domain
	$domain = get_tld($domain);

	$question=$domain; // the question we will ask

	$type="NS";
	$dns_query = new DNSQuery($dns_server);

	$result = $dns_query->Query($question,$type); // do the query

	// echo "<pre>\$result: " . print_r($result, true) . "</pre>";

	// Trap Errors
	if ( ($result===false) || ($dns_query->error!=0) ) { // error occured
		echo $dns_query->lasterror;
		exit();
	}

	// Process Results
	$result_count = $result->count; // number of results returned
	$data = array();
	for ($i=0; $i<$result_count; $i++){
		if ($result->results[$i]->typeid=="NS") {
			// echo $question." has IP address ".$result->results[$i]->data."<br>";
			// echo $result->results[$i]->string."<br>";
			$data[] = $result->results[$i]->data;
		}
	}
	return $data;
}

function get_cached_domain_info($domain, $operation, $hours = 24) {
	$current_time = time(); 
	$expire_time = $hours * 60 * 60; 
	$file_name = SCRIPT_PATH.'/cache/'.$domain.'_'.$operation.'.txt';
	// decisions, decisions
	if(file_exists($file_name) && ($current_time - $expire_time < filemtime($file_name))) {
		$file_contents = json_decode(file_get_contents($file_name));
		return json_decode(json_encode($file_contents),true);
	} else {
		if($operation == 'name_server'){
			$data['data'] = get_name_server($domain);
		} elseif($operation == 'ssl_expiration'){
			$data['data'] = get_ssl_domain_expiration('https://'.$domain);
		} elseif($operation == 'ip'){
			$data['data'] = get_ip($domain);
		} elseif($operation == 'cname'){
			$data['data'] = get_cname($domain);
		} elseif($operation == 'domain_exists'){
			$data = domain_exists($domain);
		} else {
			$data['success'] = false;
			$data['error'] = true;
			$data['message'] = "Operation incorrectly specified: ${$operation}";
			return $data;
		}
		$data['cached'] = time();
		file_put_contents($file_name, json_encode($data));
		return $data;
	}
}

function purge_cache($domain = null, $operation = null){
	if( isset($domain) && isset($operation) ){
		$file = SCRIPT_PATH.'/cache/'.$domain.'_'.$operation.'.txt';
		// echo "<pre>purging cache of file: " . print_r($file, true) . "</pre>";
		if(is_file($file))
			unlink($file);
	} else {
		$files = glob(SCRIPT_PATH.'/cache/*.txt');
		foreach($files as $file){
			if(is_file($file))
				unlink($file);
		}
	}
}