# Info

All requests are cached as simple text files

# Usage:

## Get SSL Expiration Date

```
index.php?google.com&info=ssl_expiration
```

#### Example of JSON returned

```
{
  "success": true,
  "ssl": true,
  "data": {
    "domain": "google.com",
    "expiration_date": "01/08/2020",
    "days_left": 57,
    "ip": "216.58.193.78"
  }
}

```

## Get IP address
index.php?google.com&info=ip

#### Example of JSON returned

```
{
    "success": true,
    "ssl": true,
    "data":
    {
        "ip": "216.58.193.78"
    }
}
```

## Get Name Server

```
index.php?domain=rsmmdesign.com&info=name_server
```

#### Example of JSON returned

```
{
    "cached": 1583784367,
    "success": true,
    "data": [
    	"ns-1288.awsdns-33.org", 
    	"ns-1811.awsdns-34.co.uk", 
    	"ns-300.awsdns-37.com", 
    	"ns-776.awsdns-33.net"
    ]
}
```

## Get CNAME

```
index.php?domain=www.rsmmdesign.com&info=cname
```

#### Example of JSON returned

```
{
    "cached": 1583795266,
    "success": true,
    "data":
    {
        "cname": "rsmmdesign.com"
    }
}
```

## Bypass the cache when making a query

Add `&no-cache` to the URL


## Clear all cache 

Add `&purge_cache`